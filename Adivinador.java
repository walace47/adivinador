/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adivinador;

/**
 *
 * @author joan
 */
public class Adivinador {

private int[] numero;

    public Adivinador(int[] numero) {
        this.numero = numero;
    }


    public int[] evaluacion(int[] input){
        int[] retorno = {0,0,0};
        int j = 0;
        boolean exito;
        for (int i = 0; i < input.length;i++){
            exito = false;
            j = 0;
            while(j < this.numero.length && !exito){
                
                if(input[i] == numero[j] && i == j){
                    retorno[0] +=1;
                    exito = true;
                }else if(input[i] == numero[j] && i != j){
                    retorno[1] +=1; 
                    exito = true;

                }
                j++;
            }
        }
        int suma = 0;
        for (int i = 0; i < retorno.length;i++){
            suma += retorno[i]; 
        }
        if (suma < numero.length){
            retorno[2] =numero.length - suma;
        }
        
        return retorno;
                
    }
    
}
